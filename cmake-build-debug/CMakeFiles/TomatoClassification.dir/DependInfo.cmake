# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/hieunguyen/Projects/tomato_classification/src/function.cpp" "/home/hieunguyen/Projects/tomato_classification/cmake-build-debug/CMakeFiles/TomatoClassification.dir/src/function.cpp.o"
  "/home/hieunguyen/Projects/tomato_classification/src/main.cpp" "/home/hieunguyen/Projects/tomato_classification/cmake-build-debug/CMakeFiles/TomatoClassification.dir/src/main.cpp.o"
  "/home/hieunguyen/Projects/tomato_classification/src/svm.cpp" "/home/hieunguyen/Projects/tomato_classification/cmake-build-debug/CMakeFiles/TomatoClassification.dir/src/svm.cpp.o"
  "/home/hieunguyen/Projects/tomato_classification/src/utils.cpp" "/home/hieunguyen/Projects/tomato_classification/cmake-build-debug/CMakeFiles/TomatoClassification.dir/src/utils.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "."
  "../"
  "../include"
  "/usr/local/include"
  "/usr/local/include/opencv"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
